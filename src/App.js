import React from "react";
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import './App.css';
import Login from "./component/Login";
import Signup from "./component/Signup";
import Home from "./component/Home";
import Order from "./component/Order";
import Forecast from "./component/Forecast";

function App() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path={"/login"} component={Login}/>
                <Route exact path={"/signup"} component={Signup}/>
                <Route exact path={"/"} component={Home}/>
                <Route exact path={"/order"} component={Order}/>
                <Route path={"/forecast/token/:token/city/:city/type/:type"} component={Forecast}/>
            </Switch>
        </BrowserRouter>
    );
}

export default App;
