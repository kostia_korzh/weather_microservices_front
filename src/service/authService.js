import axios from 'axios';
import Constants from "../util/Constants";

const login = (username, password) => {
    return axios.post(Constants.API_AUTH_URL + 'login', {
        username: username,
        password: password
    }).then(resp => {
        console.log(resp.data)
        if (resp.data.toString().startsWith('Token:')) {
            sessionStorage.setItem("user", JSON.stringify(resp.data));
        }
        else{
            return Promise.reject();
        }
    }).catch(() => {
        return Promise.reject();
    });
};

const logout = () => {
    sessionStorage.removeItem("user");
};

const signup = (username, password) => {
    return axios.post(Constants.API_AUTH_URL + 'signup', {
        username: username,
        password: password
    });
};

export default {
    login,
    logout,
    signup
};