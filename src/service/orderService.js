import axios from 'axios';
import Constants from "../util/Constants";
import authHeader from "../util/authHeader";

const user = sessionStorage.getItem("user");

const sendOrder = (city, type) => {
    console.log(authHeader())
    return axios.post(Constants.ORDER_URL, {
        token: user.toString().substr(8).replaceAll('"',''),
        city: city,
        type: type
    }, {headers: authHeader()});
};

export default {
    sendOrder
}
