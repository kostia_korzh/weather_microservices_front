import axios from 'axios';
import Constants from "../util/Constants";

const getForecast = (token, city, type) => {
    const path = Constants.FORECAST_URL + '?token=' + token + '&city=' + city + '&type=' + type;
    return axios.get(path);
};

export default {
    getForecast
}
