export default function authHeader() {
    const user = JSON.parse(sessionStorage.getItem("user"));

    if (user) {
        return {Authorization: 'Bearer ' + user.toString().substr(7).replaceAll('"','')}
    } else {
        return {};
    }
};