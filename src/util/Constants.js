const API_URL = "http://localhost:8090/";
const API_AUTH_URL = API_URL + 'user-service/auth/';
const ORDER_URL = API_URL+'order-service/api/orders'
const FORECAST_URL = API_URL + 'forecast-service/api/forecasts'

export default {
    API_URL,
    API_AUTH_URL,
    ORDER_URL,
    FORECAST_URL
}