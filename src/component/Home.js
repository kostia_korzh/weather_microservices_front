import React from 'react';
import Header from "./Header";
import UserHeader from "./UserHeader";

const Home = () => {

    const user = sessionStorage.getItem("user");

    return (
        <div>
            {!user &&
            <Header/>}
            {user &&
            <UserHeader/>}
            Home
        </div>
    );
};

export default Home;