import React, {useEffect, useState} from 'react';
import UserHeader from "./UserHeader";
import jwt_decode from "jwt-decode";
import ForecastService from '../service/forecastService';

const Forecast = (props) => {

    const user = sessionStorage.getItem("user");

    const [temp, setTemp] = useState(undefined);
    const [desc, setDesc] = useState(undefined);
    const [humidity, setHumidity] = useState(undefined);
    const [windSpeed, setWindSpeed] = useState(undefined);

    const usernameToken = jwt_decode(user.substr(8).replaceAll('"', ''));

    const [error, setError] = useState(false);

    const polling = sessionStorage.getItem("polling");

    useEffect(() => {
        interval()
        console.log("polling", polling);
        return () => clearInterval(interval);

    }, []);

    useEffect(() => {
        ForecastService.getForecast(usernameToken.user_name, props.match.params.city, props.match.params.type)
            .then(function (resp) {
                let data = resp.data;
                console.log(data.temperature)
                if (data.temperature == null) {
                    setError(true);
                    console.log("Error...");
                    clearInterval(interval);
                    return;
                }
                // if (data.temperature !== undefined) {
                //     console.log("Stop polling...");
                //     clearInterval(interval);
                // }
                setDesc(data.description)
                setHumidity(data.humidity)
                setTemp(data.temperature)
                setWindSpeed(data.windSpeed)
            })

    }, []);

    const interval = () => {
        setInterval(() => ForecastService.getForecast(usernameToken.user_name, props.match.params.city, props.match.params.type)
            .then(function (resp) {
                let data = resp.data;
                console.log(data.temperature)
                if (data.temperature == null) {
                    setError(true);
                    console.log("Error...");
                    clearInterval(interval);
                    return;
                }
                // if (data.temperature !== undefined) {
                //     console.log("Stop polling...");
                //     clearInterval(interval);
                // }
                setDesc(data.description)
                setHumidity(data.humidity)
                setTemp(data.temperature)
                setWindSpeed(data.windSpeed)
            }), parseInt(polling));
    }

    useEffect(() => {
        if (error) {
            setTemp("Error making forecast for you, sorry")
        }
    }, [error])

    return (
        <>
            <UserHeader/>
            <ul>
                <li>{props.match.params.city}</li>
                {temp != undefined &&
                <li>temperature: {temp}</li>}
                {desc != undefined &&
                <li> description: {desc}</li>}
                {windSpeed != undefined &&
                <li>wind speed: {windSpeed}</li>}
                {humidity != undefined &&
                <li>humidity: {humidity}</li>}
            </ul>

        </>
    );
};

export default Forecast;