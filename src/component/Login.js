import React, {useState} from 'react';
import {Button, Container, Form} from 'react-bootstrap';
import AuthService from '../service/authService';
import Header from "./Header";

const Login = (props) => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');


    return (
        <>
            <Header/>
            <Container style={{marginTop: "50px"}}>
                <Form>
                    <Form.Group>
                        <Form.Label>Enter login</Form.Label>
                        <Form.Control type="email" placeholder="login"
                                      onChange={(e) => {
                                          setUsername(e.target.value)
                                      }}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Enter password</Form.Label>
                        <Form.Control type="password" placeholder="password"
                                      onChange={(e) => {
                                          setPassword(e.target.value)
                                      }}/>
                    </Form.Group>
                    <Button variant="primary" type="submit"
                            onClick={(e) => {
                                e.preventDefault();
                                AuthService.login(username, password).then(() => {
                                    props.history.push("/");
                                    window.location.reload();
                                }).catch(()=>{
                                    alert('Error during authentication')
                                })
                            }}>
                        Login
                    </Button>
                </Form>
            </Container>
        </>
    );
};

export default Login;