import React from "react";
import {Nav, Navbar} from "react-bootstrap";
import AuthService from "../service/authService";

const UserHeader = () => {

    return (
        <Navbar bg="light">
            <Nav className="mr-auto">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/order">Get Forecast</Nav.Link>
            </Nav>
            <Nav className="justify-content-end">
                <Nav.Link href="/"
                          onClick={() => {
                              AuthService.logout();
                          }}>Logout</Nav.Link>
            </Nav>
        </Navbar>
    );

};

export default UserHeader;