import React, {useState} from 'react';
import {Button, Container, Form} from "react-bootstrap";
import OrderService from "../service/orderService";
import UserHeader from "./UserHeader";
import jwt_decode from "jwt-decode";

const Order = (props) => {

    const user = sessionStorage.getItem("user");

    const [city, setCity] = useState('');
    const [type, setType] = useState("BASE");
    const [polling, setPolling] = useState(10);

    return (
        <>
            <UserHeader/>
            <Container style={{marginTop: "50px"}}>
                <Form>
                    <Form.Group>
                        <Form.Label>Enter city</Form.Label>
                        <Form.Control type="text" placeholder="city"
                                      onChange={(e) => {
                                          setCity(e.target.value)
                                      }}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Enter polling time</Form.Label>
                        <Form.Control type="text" placeholder="polling"
                                      onChange={(e) => {
                                          setPolling(e.target.value)
                                      }}/>
                    </Form.Group>
                    <Form.Control as="select" defaultValue="Choose type"
                                  onChange={(e) => {
                                      setType(e.target.value)
                                  }}>
                        <option value={"BASE"}>Base</option>
                        <option value={"USUAL"}>Usual</option>
                        <option value={"FULL"}>Full</option>
                    </Form.Control>
                    <Button variant="primary" type="submit"
                            onClick={(e) => {
                                e.preventDefault();
                                OrderService.sendOrder(city, type).then(() => {
                                    sessionStorage.setItem("polling", polling.toString(10));
                                    let usernameToken = jwt_decode(user.substr(8).replaceAll('"', ''));
                                    console.log(usernameToken);
                                    console.log(usernameToken.user_name);
                                    let url = '/forecast/token/' + usernameToken.user_name + '/city/' + city +
                                        '/type/' + type
                                    console.log(url)
                                    props.history.push(url);
                                    window.location.reload();
                                });
                            }}>
                        - Get Forecast -
                    </Button>
                </Form>
            </Container>
        </>
    );
};

export default Order;